const slue = require('slue');
const sequence = require('./index');

slue.task('a', function() {
    console.log('******* aaa ********');
});

slue.task('b', function() {
    console.log('******* bbb ********');
});

let promise = new Promise(function(resolve, reject) {
    resolve();
});

//sequence().call(slue, ['a', 'b']);
sequence(['a', 'b']).call(slue);