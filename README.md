# slue-sequence

Run slue tasks in order.

## Usage

```javascript
const slue = require('slue');
const sequence = require('slue-sequence');

slue.task('a', function() {

});

slue.task('a', function() {

});

slue.task('dev', sequence(['a', 'b']));
```