const Bluebird = require('bluebird');

module.exports = function(taskList) {
    let slueSequence = function(taskList) {
        let slue = require('slue');
        
        let promise = new Bluebird(function(resolve) {
            resolve();
        });
        taskList.forEach(function(taskName) {
            promise = promise.then(function() {
                return slue.runOneTask(taskName)
            });
        });

        return promise;
    }

    return function() {
        return slueSequence(taskList);
    };
}